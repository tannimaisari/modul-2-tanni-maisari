package modul2.tannimaisari.marvelapps;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class CheckoutActivity extends AppCompatActivity {

    TextView aa,bb,cc,dd,ee,pp;

    String tjuan,ber,pul,tiket,saldowal,total,ganti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        aa = findViewById(R.id.tujuan);
        bb = findViewById(R.id.tglkeber);
        cc = findViewById(R.id.tglkepul);
        dd = findViewById(R.id.jmlhtiket);
        ee = findViewById(R.id.hargatot);
        pp = findViewById(R.id.jj);

        tjuan = getIntent().getStringExtra("tujuan");
        ber = getIntent().getStringExtra("berangkat");
        pul = getIntent().getStringExtra("pulang");
        saldowal = getIntent().getStringExtra("saldoawal");
        total = getIntent().getStringExtra("total");
        tiket = getIntent().getStringExtra("tiket");
        ganti = getIntent().getStringExtra("ganti");

        if (ganti.equals("Pulper")) {

            cc.setVisibility(View.VISIBLE);
            pp.setVisibility(View.VISIBLE);
            aa.setText(tjuan);
            bb.setText(ber);
            cc.setText(pul);
            dd.setText(tiket);
            ee.setText(total);
        } else {
            aa.setText(tjuan);
            bb.setText(ber);
            dd.setText(tiket);
            ee.setText(total);
        }


    }

    public void konfirm(View view) {
        Intent done = new Intent(CheckoutActivity.this, MainActivity.class);
        Toast.makeText(this,"Tengkyu bby",Toast.LENGTH_SHORT).show();
        startActivity(done);
    }
}

package modul2.tannimaisari.marvelapps;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import static java.lang.Boolean.TRUE;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    TextView nminal, tanggalber, wktuber, tanggalpul, wktupul;
    EditText jmlh;
    Spinner tjuan;
    Switch gnti;

    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day;

    String aa, bb;

    int nominalawal, nominaltambah, harga, hitung, jml, kali;

    private static final String[] rute = {"Jakarta (Rp. 80.000)",
            "Malang (Rp. 200.000)", "Bogor (Rp. 70.000)"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        nminal = findViewById(R.id.angka);
        tanggalber = findViewById(R.id.tglber);
        wktuber = findViewById(R.id.waktuber);
        tanggalpul = findViewById(R.id.tglpul);
        wktupul = findViewById(R.id.waktupul);
        jmlh = findViewById(R.id.jumlah);
        tjuan = findViewById(R.id.spinner);
        gnti = findViewById(R.id.ganti);

        nminal.setText("0");
        nominalawal = Integer.parseInt(String.valueOf(nminal.getText()));


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_spinner_item, rute);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tjuan.setAdapter(adapter);
        tjuan.setOnItemSelectedListener(this);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        gnti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    wktupul.setVisibility(View.VISIBLE);
                    tanggalpul.setVisibility(View.VISIBLE);
                } else {
                    wktupul.setVisibility(View.INVISIBLE);
                    tanggalpul.setVisibility(View.INVISIBLE);
                }

            }
        });
    }

    public void isi(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Masukkan Jumlah Saldo");

        View viewInflated = LayoutInflater.from(getApplicationContext()).inflate(R.layout.inpunominal, null, false);
        final EditText input1 = (EditText) viewInflated.findViewById(R.id.input);
        builder.setView(viewInflated);

        builder.setPositiveButton("TAMBAH SALDO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                aa = input1.getText().toString();
                nominaltambah = Integer.parseInt(nominalawal + aa);
                nminal.setText(String.valueOf(nominaltambah));
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (position) {
            case 0:
                harga = 80000;
                break;
            case 1:
                break;
            case 2:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void isitanggal(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(),
                "datepicker");
    }

    public void isiwaktu(View view) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(),
                "datepicker");
    }

    public void isitanggalpul(View view) {
        DialogFragment newFragment = new DatePickerFragment1();
        newFragment.show(getSupportFragmentManager(),
                "datepicker");
    }

    public void isiwaktupul(View view) {
        DialogFragment newFragment = new TimePickerFragment1();
        newFragment.show(getSupportFragmentManager(),
                "datepicker");

    }

    public void showDate(int year, int month, int day) {
        String month_string = Integer.toString(month + 1);
        String day_string = Integer.toString(day);
        String year_string = Integer.toString(year);

        tanggalber.setText(new StringBuilder().append(day_string).append("/")
                .append(month_string).append("/").append(year_string));
    }

    public void showTime(int hourofday, int minute) {
        String hour_string = Integer.toString(hourofday);
        String minute_string = Integer.toString(minute);

        wktuber.setText(new StringBuilder().append(hour_string).append(":")
                .append(minute_string).append(" WIB"));
    }

    public void showDate1(int year, int month, int day) {
        String month_string = Integer.toString(month + 1);
        String day_string = Integer.toString(day);
        String year_string = Integer.toString(year);

        tanggalpul.setText(new StringBuilder().append(day_string).append("/")
                .append(month_string).append("/").append(year_string));
    }

    public void showTime1(int hourofday, int minute) {
        String hour_string = Integer.toString(hourofday);
        String minute_string = Integer.toString(minute);

        wktupul.setText(new StringBuilder().append(hour_string).append(":")
                .append(minute_string).append(" WIB"));
    }

    public void beli(View view) {
        if (tjuan.getSelectedItemPosition() == 0) {
            if (nominaltambah < 80000) {
                Toast.makeText(this, "Maaf Saldo anda kurang Top Up terlebih dahulu", Toast.LENGTH_SHORT).show();
            } else {
                if (gnti.isChecked()) {
                    kali = 2 * 80000 * Integer.parseInt(jmlh.getText().toString());
                    hitung = nominaltambah - kali;
                    if (kali > nominaltambah) {
                        Toast.makeText(this, "Maaf Saldo anda kurang Top Up terlebih dahulu", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent oo = new Intent(MainActivity.this, CheckoutActivity.class);
                        oo.putExtra("tujuan", "JAKARTA");
                        oo.putExtra("ganti", "Pulper");
                        oo.putExtra("berangkat", tanggalber.getText() + "-" + wktuber.getText());
                        oo.putExtra("pulang", tanggalpul.getText() + "-" + wktupul.getText());
                        oo.putExtra("tiket", jmlh.getText().toString());
                        oo.putExtra("total", String.valueOf(kali));
                        oo.putExtra("saldoawal", nominaltambah);
                        startActivity(oo);
                    }
                } else {
                    kali = 1 * 80000 * Integer.parseInt(jmlh.getText().toString());
                    hitung = nominaltambah - kali;
                    if (kali > nominaltambah) {
                        Toast.makeText(this, "Maaf Saldo anda kurang Top Up terlebih dahulu", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent oo = new Intent(MainActivity.this, CheckoutActivity.class);
                        oo.putExtra("tujuan", "JAKARTA");
                        oo.putExtra("ganti", "Pergi");
                        oo.putExtra("berangkat", tanggalber.getText() + "-" + wktuber.getText());
                        oo.putExtra("tiket", jmlh.getText().toString());
                        oo.putExtra("total", String.valueOf(kali));
                        oo.putExtra("saldoawal", nominaltambah);
                        startActivity(oo);
                    }
                }
            }

        } else if (tjuan.getSelectedItemPosition() == 1) {
            if (nominaltambah < 200000) {
                Toast.makeText(this, "Maaf Saldo anda kurang Top Up terlebih dahulu", Toast.LENGTH_SHORT).show();
            } else {
                if (gnti.isChecked()) {
                    kali = 2 * 200000 * Integer.parseInt(jmlh.getText().toString());
                    hitung = nominaltambah - kali;
                    if (kali > nominaltambah) {
                        Toast.makeText(this, "Maaf Saldo anda kurang Top Up terlebih dahulu", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent oo = new Intent(MainActivity.this, CheckoutActivity.class);
                        oo.putExtra("tujuan", "MALANG");
                        oo.putExtra("ganti", "Pulper");
                        oo.putExtra("berangkat", tanggalber.getText() + "-" + wktuber.getText());
                        oo.putExtra("pulang", tanggalpul.getText() + "-" + wktupul.getText());
                        oo.putExtra("tiket", jmlh.getText().toString());
                        oo.putExtra("total", String.valueOf(kali));
                        oo.putExtra("saldoawal", nominaltambah);
                        startActivity(oo);
                    }
                } else {
                    kali = 1 * 200000 * Integer.parseInt(jmlh.getText().toString());
                    hitung = nominaltambah - kali;
                    if (kali > nominaltambah) {
                        Toast.makeText(this, "Maaf Saldo anda kurang Top Up terlebih dahulu", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent oo = new Intent(MainActivity.this, CheckoutActivity.class);
                        oo.putExtra("tujuan", "MALANG");
                        oo.putExtra("ganti", "Pergi");
                        oo.putExtra("berangkat", tanggalber.getText() + "-" + wktuber.getText());
                        oo.putExtra("tiket", jmlh.getText().toString());
                        oo.putExtra("total", String.valueOf(kali));
                        oo.putExtra("saldoawal", nominaltambah);
                        startActivity(oo);
                    }
                }
            }

        } else if (tjuan.getSelectedItemPosition() == 2) {
            if (nominaltambah < 70000) {
                Toast.makeText(this, "Maaf Saldo anda kurang Top Up terlebih dahulu", Toast.LENGTH_SHORT).show();
            } else {
                if (gnti.isChecked()) {
                    kali = 2 * 70000 * Integer.parseInt(jmlh.getText().toString());
                    hitung = nominaltambah - kali;
                    if (kali > nominaltambah) {
                        Toast.makeText(this, "Maaf Saldo anda kurang Top Up terlebih dahulu", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent oo = new Intent(MainActivity.this, CheckoutActivity.class);
                        oo.putExtra("tujuan", "BOGOR");
                        oo.putExtra("ganti", "Pulper");
                        oo.putExtra("berangkat", tanggalber.getText() + "-" + wktuber.getText());
                        oo.putExtra("pulang", tanggalpul.getText() + "-" + wktupul.getText());
                        oo.putExtra("tiket", jmlh.getText().toString());
                        oo.putExtra("total", String.valueOf(kali));
                        oo.putExtra("saldoawal", nominaltambah);
                        startActivity(oo);
                    }
                } else {
                    kali = 1 * 70000 * Integer.parseInt(jmlh.getText().toString());
                    hitung = nominaltambah - kali;
                    if (kali > nominaltambah) {
                        Toast.makeText(this, "Maaf Saldo anda kurang Top Up terlebih dahulu", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent oo = new Intent(MainActivity.this, CheckoutActivity.class);
                        oo.putExtra("tujuan", "BOGOR");
                        oo.putExtra("ganti", "Pergi");
                        oo.putExtra("berangkat", tanggalber.getText() + "-" + wktuber.getText());
                        oo.putExtra("tiket", jmlh.getText().toString());
                        oo.putExtra("total", String.valueOf(kali));
                        oo.putExtra("saldoawal", nominaltambah);
                        startActivity(oo);
                    }
                }
            }
        }
    }
}
